#include <Wire.h>
#include <Arduino.h>
#include <analogWrite.h>
#include "PS2X_lib.h"
#include "CytronMotorDriver.h"
//#include <Adafruit_GFX.h>
//#include <Adafruit_SSD1306.h>
//#include <SPI.h>

#define pressures   true
#define rumble      true

#define PS2_DAT        16  // Green
#define PS2_CMD        19  // Brown
#define PS2_SEL        18  // Blue
#define PS2_CLK        17  // Orange

//#define SCREEN_WIDTH 128
//#define SCREEN_HEIGHT 64

// Configure the motor driver.
//CytronMD Drive(PWM_PWM, 10, 11); //for Arduino Uno
//CytronMD Steer(PWM_PWM, 3, 9);   //for Arduino Uno

CytronMD Drive(PWM_PWM, 4, 0);
CytronMD Steer(PWM_PWM, 2, 15);

PS2X ps2x;

int error = 1;
byte type = 0;
byte vibrate = 0;

void initPS2X() {
  do {
    //setup pins and settings: GamePad(clock, command, attention, data, Pressures?, Rumble?) check for error
    error = ps2x.config_gamepad(PS2_CLK, PS2_CMD, PS2_SEL, PS2_DAT, pressures, rumble);
    if (error == 0) {
      Serial.println("Configured successful");
      break;
    } else {
      Serial.print(error);
      Serial.print(".");
      delay(100);
    }
  } while (1);

  if (error == 0) {
    Serial.print("Found Controller, configured successful ");
    Serial.print("pressures = ");
    if (pressures)
      Serial.println("true ");
    else
      Serial.println("false");
    Serial.print("rumble = ");
    if (rumble)
      Serial.println("true)");
    else
      Serial.println("false");
    Serial.println("Try out all the buttons, X will vibrate the controller, faster as you press harder;");
    Serial.println("holding L1 or R1 will print out the analog stick values.");
    Serial.println("Note: Go to www.billporter.info for updates and to report bugs.");
  }
  else if (error == 1)
    Serial.println("No controller found, check wiring, see readme.txt to enable debug. visit www.billporter.info for troubleshooting tips");

  else if (error == 2)
    Serial.println("Controller found but not accepting commands. see readme.txt to enable debug. Visit www.billporter.info for troubleshooting tips");

  else if (error == 3)
    Serial.println("Controller refusing to enter Pressures mode, may not support it. ");

  Serial.print(ps2x.Analog(1), HEX);

  type = ps2x.readType();
  switch (type) {
    case 0:
      Serial.print("Unknown Controller type found ");
      break;
    case 1:
      Serial.print("DualShock Controller found ");
      break;
    case 2:
      Serial.print("GuitarHero Controller found ");
      break;
    case 3:
      Serial.print("Wireless Sony DualShock Controller found ");
      break;
  }
}

// The setup routine runs once when you press reset.
void setup() {

  Serial.begin(115200);
  Serial.print("Setup: ");
  //  pinMode(D1, OUTPUT);
  //  pinMode(D2, OUTPUT);
  //  pinMode(D3, OUTPUT);
  //  pinMode(D4, OUTPUT);
  Serial.println("Define output pins: Done!");

  initPS2X();
}


// The loop routine runs over and over again forever.
void loop() {

  if (error == 1) //skip loop if no controller found
    return;

  ps2x.read_gamepad(false, vibrate); //read controller and set large motor to spin at 'vibrate' speed
  int ry = (((ps2x.Analog(PSS_RY)) - 127) * 100) / 128 ;
  Serial.print("ry - ");
  Serial.println(ry);
  delay(10);
  int ly = (((ps2x.Analog(PSS_LY)) - 127) * 100) / 128 ;
  Serial.print("ly - ");
  Serial.println(ly);
  delay(10);

  setMotor(ry, ly);

  //  testPS2X();
  //  delay(500);
  //  setMotor(150, 127);
  //  delay(2000);
  //  setMotor(127, 150);
  //  delay(2000);

}

void setMotor(int moof, int turn) {

  if (moof >= -5 && moof <= 5) {
    Drive.setSpeed(0);
  }
  else if (moof > 5) {
    Drive.setSpeed(map(moof, -130, 130, -255, 255));
  }
  else if (moof < -5) {
    Drive.setSpeed(map(moof, -130, 130, -255, 255));
  }

  if (turn >= -5 && turn <= 5) {
    Steer.setSpeed(0);
  }
  else if (turn > 5) {
    Steer.setSpeed(map(turn, -130, 130, -255, 255));
  }

  else if (turn < -5) {
    Steer.setSpeed(map(turn, -130, 130, -255, 255));
  }

}

void testPS2X() {
  ps2x.read_gamepad(false, vibrate); //read controller and set large motor to spin at 'vibrate' speed

  if (ps2x.Button(PSB_START))        //will be TRUE as long as button is pressed
    Serial.println("Start is being held");
  if (ps2x.Button(PSB_SELECT))
    Serial.println("Select is being held");

  if (ps2x.Button(PSB_PAD_UP)) {     //will be TRUE as long as button is pressed
    Serial.print("Up held this hard: ");
    Serial.println(ps2x.Analog(PSAB_PAD_UP), DEC);
  }
  if (ps2x.Button(PSB_PAD_RIGHT)) {
    Serial.print("Right held this hard: ");
    Serial.println(ps2x.Analog(PSAB_PAD_RIGHT), DEC);
  }
  if (ps2x.Button(PSB_PAD_LEFT)) {
    Serial.print("LEFT held this hard: ");
    Serial.println(ps2x.Analog(PSAB_PAD_LEFT), DEC);
  }
  if (ps2x.Button(PSB_PAD_DOWN)) {
    Serial.print("DOWN held this hard: ");
    Serial.println(ps2x.Analog(PSAB_PAD_DOWN), DEC);
  }

  vibrate = ps2x.Analog(PSAB_CROSS);  //this will set the large motor vibrate speed based on how hard you press the blue (X) button
  if (ps2x.NewButtonState()) {        //will be TRUE if any button changes state (on to off, or off to on)
    if (ps2x.Button(PSB_L3))
      Serial.println("L3 pressed");
    if (ps2x.Button(PSB_R3))
      Serial.println("R3 pressed");
    if (ps2x.Button(PSB_L2))
      Serial.println("L2 pressed");
    if (ps2x.Button(PSB_R2))
      Serial.println("R2 pressed");
    if (ps2x.Button(PSB_TRIANGLE))
      Serial.println("Triangle pressed");
  }

  if (ps2x.ButtonPressed(PSB_CIRCLE))              //will be TRUE if button was JUST pressed
    Serial.println("Circle just pressed");
  if (ps2x.NewButtonState(PSB_CROSS))              //will be TRUE if button was JUST pressed OR released
    Serial.println("X just changed");
  if (ps2x.ButtonReleased(PSB_SQUARE))             //will be TRUE if button was JUST released
    Serial.println("Square just released");

  if (ps2x.Button(PSB_L1) || ps2x.Button(PSB_R1)) { //print stick values if either is TRUE
    Serial.print("Stick Values:");
    Serial.print(ps2x.Analog(PSS_LY), DEC); //Left stick, Y axis. Other options: LX, RY, RX
    Serial.print(",");
    Serial.print(ps2x.Analog(PSS_LX), DEC);
    Serial.print(",");
    Serial.print(ps2x.Analog(PSS_RY), DEC);
    Serial.print(",");
    Serial.println(ps2x.Analog(PSS_RX), DEC);
  }
}
